# SP3 - Cards

Дорошенко Вікторія: modal + forms (sign in & create visit), rendering of cards after creation, "show more" btn realization, "no items have been added" feature;

Кирилюк Ольга: edit-btn realization, modifying show-more with show-less

Лушников Валентин: filter cards by name feature, render all cards, doctor-api: getCards / deleteCards

Ромаан Орган: gulp, ApiDoctorService, modifying displayallCards, modifying select field visit form
